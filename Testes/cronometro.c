#include <time.h>
#include <stdlib.h>
#include <stdio.h>

const int TAM = 100;


void main(){
    int arranjo[TAM];

    clock_t comeco = clock();

    //Escrever aqui a funcao para ser cronometrada
    for(int i = 0; i < TAM; i++){
        arranjo[i] = rand() % 100;
    }


    clock_t fim = clock();
    double total = (clock_t)(fim - comeco) / (double)CLOCKS_PER_SEC;

    printf("\n%lf segundos!\n", total);
}
