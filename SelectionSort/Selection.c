#include <stdlib.h>
#include <stdio.h>

 void swap (int indice, int i, int array[]) {
 int AUX;

  AUX = array[i];
  array[i] = array[indice];
  array[indice] = AUX;
}

void gerador(int array[], int n){
    for(int i = 0 ; i < n ; i++){
        array[i] = rand() % 100;
    }
}

void mostrar(int array[], int n){
printf("[ ");

for ( int i = 0; i < n; i++) {
  printf("%d ",array[i]);
}
printf("]");
}


// Algoritmo de ordenacao
void selectionSort(int array[], int n) {
  int i;
   for (i = 0; i < (n - 1); i++) {
      int indice = i;
      int j;
      for (j = (i + 1); j < n; j++){
         if (array[indice] > array[j]){
            indice = j;
         }
      }
      swap(indice, i, array);
   }
}

int main() {
  int n = 6;
  int array[n];

   gerador(array,n);
   mostrar(array,n);
   selectionSort(array,n);
   mostrar(array,n);
}
