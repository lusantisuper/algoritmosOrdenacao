import java.util.Random;

public class SelectionSort{
  final static int TAM = 6;

public static void main(String[] args){
  int[] numeros = new int[TAM];

  //Gerando os valores aleatorios
  Gerador gerador = new Gerador(numeros, TAM);

  System.out.println("Numeros aleatorios gerados:");
  gerador.mostrar();


  //Instanciando a classe Bubble para organizar os valores
  Insert insert = new Insert(numeros, TAM);
  insert.selectionSort();


  System.out.println("\nNumeros organizados:");
  gerador.mostrar();
}

}

class Insert {
   private static int TamArray;
   private static int[] numeros;

  public Insert(int[] array, int n){
    TamArray = n;
    numeros = array;
  }

  public void selectionSort() {
   int i;
    for (i = 0 ; i < (TamArray-1) ; i++) {
    int indice = i;
    int j;
    for (j = (i + 1) ; j < TamArray ; j++)
    if (numeros[indice] > numeros[j]) {
    indice = j;
    }
    swap(indice,i);
    }
  }

  public void swap(int indice, int i){
  int AUX;

  AUX = numeros[i];
  numeros[i] = numeros[indice];
  numeros[indice] = AUX;

  }

}

class Gerador {
    private static int tamArranjo;
    private static int[] numeros;
    private static Random gerador = new Random();

    public Gerador(int[] arranjo, int tam) {
        tamArranjo = tam;
        numeros = arranjo;
        gerar();
    }

    public static void gerar(){
        for (int i = 0; i < tamArranjo; i++) {
            numeros[i] = Math.abs(gerador.nextInt()) % 100;
        }
    }

    public void mostrar(){
        System.out.print("[");
        for (int i = 0; i < tamArranjo; i++) {
            System.out.print(" " + numeros[i] + " ");
        }
        System.out.print("]\n");
    }
}
