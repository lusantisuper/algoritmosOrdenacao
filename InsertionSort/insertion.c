//Bibliotecas importantes
#include <stdio.h>
#include "../Ferramentas/randomGenerator.h"
#include "InsertionSort.h"
#include "Cronometro.h"

//Constantes globais
const int TAM = 10000;


//Funcao padrao para execucao de codigos
void ORQUESTRADOR(int numeros[]){
    //Mostrar valores que ja foram gerados
    printf("\nValores gerados:\n");
    for(int i = 0; i < TAM; i++){
        printf("%i\n", numeros[i]);
    }

    //Chamar a funcao InsertionSort
    double tempoTotal = Cronometrar(numeros, TAM);

    //Mostrar valores ordenados
    printf("\nValores ordenados:\n");
    for(int i = 0; i < TAM; i++){
        printf("%i\n", numeros[i]);
    }
    printf("\n%f milisegundos para ordenar esses valores!", tempoTotal);
}


int main(){
    int numeros[TAM];

    gerador(numeros, TAM); //Gerar um vetor com valores aleatorios

    //Chamar a funcao para fazer as execucoes
    ORQUESTRADOR(numeros);

    //Finalizar o programa
    printf("\n");
    return 0;
}
