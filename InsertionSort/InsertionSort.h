//Insertion sort

void InsertionSort(int arranjo[], int tamanhoArranjo){
    for(int i = 1; i < tamanhoArranjo; i++){
        int tmp = arranjo[i];
        int j = i - 1;
        while(j >= 0 && arranjo[j] > tmp){
            arranjo[j+1] = arranjo[j];
            j--;
        }
        arranjo[j + 1] = tmp;
    }
}
