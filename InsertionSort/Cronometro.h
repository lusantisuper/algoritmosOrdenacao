#include <time.h>

double Cronometrar(int arranjo[], int tamanhoArranjo){
    clock_t comeco = clock();

    //Escrever aqui a funcao para ser cronometrada
    InsertionSort(arranjo, tamanhoArranjo);

    //Finalizar a contagem de tempo
    clock_t fim = clock();
    double total = (fim - comeco) / (double)CLOCKS_PER_SEC * 1000.0;

    return total;
}
