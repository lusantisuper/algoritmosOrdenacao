import java.util.Random;

public class Quicksort {
    final static int TAM = 10;
    public static void main(String[] args){
        int[] numeros = new int[TAM];
        Gerador gerador = new Gerador(numeros);

        gerador.mostrar();
    }
}


class Gerador {
    private static int tamArranjo;
    private static int[] numeros;
    private static Random gerador = new Random();

    public Gerador(int[] arranjo) {
        tamArranjo = arranjo.length;
        numeros = arranjo;
        gerar();
    }

    public static void gerar(){
        for (int i = 0; i < tamArranjo; i++) {
            numeros[i] = Math.abs(gerador.nextInt()) % 100;
        }
    }

    public void mostrar(){
        System.out.print("[");
        for (int i = 0; i < tamArranjo; i++) {
            System.out.print(" " + numeros[i] + " ");
        }
        System.out.print("]\n");
    }
}
