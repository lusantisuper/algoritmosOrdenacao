import java.util.Random;

class Gerador {
    private static int tamArranjo;
    private static int[] numeros;
    private static Random gerador = new Random();

    public Gerador(int[] arranjo, int tam) {
        tamArranjo = tam;
        numeros = arranjo;
        gerar();
    }

    public static void gerar(){
        for (int i = 0; i < tamArranjo; i++) {
            numeros[i] = Math.abs(gerador.nextInt()) % 100;
        }
    }

    public void mostrar(){
        System.out.print("[ ");
        for (int i = 0; i < tamArranjo; i++) {
            System.out.print(" " + numeros[i] + " ");
        }
        System.out.print(" ]");
    }
}
