import java.util.Random;

public class Bubblesort{
    final static int TAM = 100000;

    public static void main(String[] args){
        int[] numeros = new int[TAM];

        //Gerando os valores aleatorios
        Gerador gerador = new Gerador(numeros, TAM);

        System.out.println("Numeros aleatorios gerados:");
        gerador.mostrar();


        //Instanciando a classe Bubble para organizar os valores
        Bubble bolha = new Bubble(numeros, TAM);
        bolha.sort();


        System.out.println("\nNumeros organizados:");
        gerador.mostrar();
    }
}


class Bubble{
    private static int tamArranjo;
    private static int[] numeros;

    public Bubble(int[] arranjo, int tam) {
        tamArranjo = tam;
        numeros = arranjo;
    }

    public void sort(){
        for (int i = tamArranjo - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (numeros[j] > numeros[j + 1]) {
                    swap(j, j+1);
				}
			}
		}
    }

    public void swap(int primeiro, int segundo){
        int tmp = numeros[primeiro];
        numeros[primeiro] = numeros[segundo];
        numeros[segundo] = tmp;
    }
}


class Gerador {
    private static int tamArranjo;
    private static int[] numeros;
    private static Random gerador = new Random();

    public Gerador(int[] arranjo, int tam) {
        tamArranjo = tam;
        numeros = arranjo;
        gerar();
    }

    public static void gerar(){
        for (int i = 0; i < tamArranjo; i++) {
            numeros[i] = Math.abs(gerador.nextInt()) % 100;
        }
    }

    public void mostrar(){
        System.out.print("[");
        for (int i = 0; i < tamArranjo; i++) {
            System.out.print(" " + numeros[i] + " ");
        }
        System.out.print("]\n");
    }
}
